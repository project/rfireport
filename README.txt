
Overview:
--------
This module adds a "RFI Report" and " RFI Statistics" link in the Reports section of the administration pages. The RFI Report option will search the log created by the dblog (core) module for Remote File Inclusion attempts and displays full details about each attempt in a table. The RFI Statistics link will display the daily RFI attempts and top 10 RFI attacking hosts in two charts.

Only visible for users with the "access site reports" privilege.

Installation:
------------
Installation is as simple as copying the module into your modules directory, then enabling the module at:

Administer >> Site Building >> Modules

Go to

Administer >> Reports

and you'll see the "RFI Report" and "RFI Statistics" link

Go to

Site configuration >> RFI Report

and you will see the "RFI Report" settings link

